var objEvents = {
	sendVideoLog : false
};

var objForm = {
	objElement : null,

	getValue: function(objElement) {
		return $.trim(objElement.val());
	},

	notEmpty: function(strValue) {
		return (strValue.length >= 1);
	},

	isEmail: function(strValue) {
		var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
    	return pattern.test(strValue);
	},

	isAlpha: function(strValue) {
		var regexp = /[^a-zA-Z ]/g;
		return !strValue.match(regexp);
	},

	isNumeric: function(strValue) {
		var regexp = /[^0-9\. \(\)]/g;
		return !strValue.match(regexp);
	},

	isEqual: function(strValue, strMatchValue) {
		return (strValue === strMatchValue);
	},

	verifyPass: function(strValue) {
		$.ajax({
		  method: 'POST',
		  url: Drupal.settings.basePath + 'check-pass',
		  data: { 'pass': strValue }
		})
		.done(function( result ) {
			console.log(result);
			return result;
		});
	},

	validate: function(objElement) {
		var _this = this;
		objElement = objElement;
		if (objElement.data('rules').length >= 1) {
			var arrRules = objElement.data('rules').split('|');
			var strValue = this.getValue(objElement);
			var numError = 0;

			$.each(arrRules, function(index, rule) {
				switch (rule) {
					case 'required':
						if (!_this.notEmpty(strValue)) {
							numError += 1;
							console.log('.validate-note.' + objElement.attr('name') + '-' + rule);
							$('.validate-note.' + objElement.attr('name') + '-' + rule).addClass('active');
						}
						else {
							$('.validate-note.' + objElement.attr('name') + '-' + rule).removeClass('active');
						}
						break;

					case 'email':
						if (!_this.isEmail(strValue)) {
							numError += 1;
							$('.validate-note.' + objElement.attr('name') + '-' + rule).addClass('active');
						}
						else {
							$('.validate-note.' + objElement.attr('name') + '-' + rule).removeClass('active');
						}
						break;

					case 'number':
						if (
							!_this.isNumeric(strValue) ||
							( 
								objElement.hasClass('phone-field') && 
								strValue.length != 14
							)
						) {
							numError += 1;
							$('.validate-note.' + objElement.attr('name') + '-' + rule).addClass('active');
						}
						else {
							$('.validate-note.' + objElement.attr('name') + '-' + rule).removeClass('active');
						}
						break;

					case 'alpha':
						if (!_this.isAlpha(strValue)) {
							numError += 1;
							$('.validate-note.' + objElement.attr('name') + '-' + rule).addClass('active');
						}
						else {
							$('.validate-note.' + objElement.attr('name') + '-' + rule).removeClass('active');
						}
						break;

					case 'checked':
						if (!objElement.is(":checked")) {
							numError += 1;
							$('.validate-note.' + objElement.attr('name') + '-' + rule).addClass('active');
						}
						else {
							$('.validate-note.' + objElement.attr('name') + '-' + rule).removeClass('active');
						}
						break;

					case 'size':
						var min = objElement.data('min');
						if (min >= 1) {
							if (strValue.length < min) {
								numError += 1;
								$('.validate-note.' + objElement.attr('name') + '-' + rule).addClass('active');
							}
							else {
								$('.validate-note.' + objElement.attr('name') + '-' + rule).removeClass('active');
							}
						}

						var max = objElement.data('max');
						if (max >= 1) {
							if (strValue.length > max) {
								numError += 1;
								$('.validate-note.' + objElement.attr('name') + '-' + rule).addClass('active');
							}
							else {
								$('.validate-note.' + objElement.attr('name') + '-' + rule).removeClass('active');
							}
						}
						break;

					case 'match':
						var objMatchElement = $('#' + objElement.data('match'));
						var floMatch = _this.isEqual(strValue, objMatchElement.val());

						if(!floMatch) {
							numError += 1;
							objMatchElement.addClass('error-field');
							$('.validate-note.' + objElement.attr('name') + '-' + rule).addClass('active');
						}
						else {
							objMatchElement.removeClass('error-field');
							$('.validate-note.' + objElement.attr('name') + '-' + rule).removeClass('active');
						}
						break;

					case 'dependent':
						var objMatchElement = $('#' + objElement.data('dependent'));

						if(objElement.is(':checked') && !objMatchElement.is(':checked')) {
							numError += 1;
							objMatchElement.addClass('error-field');
							$('.validate-note.' + objElement.attr('name') + '-' + rule).addClass('active');
						}
						else {
							objMatchElement.removeClass('error-field');
							$('.validate-note.' + objElement.attr('name') + '-' + rule).removeClass('active');
						}
						break;

					case 'dependent-select':
						var objMatchElement = $('#' + objElement.data('dependent-select'));

						if(!objForm.validate(objMatchElement)) {
							numError += 1;
						}
						break;

					case 'verifypass':
						var objMatchElement = $('#' + objElement.data('match'));
						var floMatch = _this.isEqual(strValue, objMatchElement.val());

						if(!floMatch) {
							numError += 1;
							objMatchElement.addClass('error-field');
							$('.validate-note.' + objElement.attr('name') + '-' + rule).addClass('active');
						}
						else {
							objMatchElement.removeClass('error-field');
							$('.validate-note.' + objElement.attr('name') + '-' + rule).removeClass('active');
						}

						var objVerifyElement = $('#password-old');
						var floVerified = _this.verifyPass(objMatchElement.val());

						if(!floVerified) {
							numError += 1;
							objVerifyElement.addClass('error-field');
							$('.validate-note.' + objElement.attr('name') + '-' + rule).addClass('active');
						}
						else {
							objVerifyElement.removeClass('error-field');
							$('.validate-note.' + objElement.attr('name') + '-' + rule).removeClass('active');
						}
						break;
				}
			});

			// add error class for label styling
			if(numError > 0) {
				objElement.closest('li').addClass('has-error');
			} else {
				objElement.closest('li').removeClass('has-error');
			}

			return (numError <= 0);
		}
		else {
			return true;
		}
	}
}

var objTheme = {
	setActiveNav: function() {
		var strURI = $('body').data('uri');
		$('a[data-target="' + strURI + '"]').parent('li').addClass('active');

		if(strURI.indexOf('medicines/') > -1) {
			$('a[data-target="medicines"]').parent('li').addClass('active');
		}
	},

	setActivityHeight: function() {
		var windowWidth = $(window).width();
		var target = $('.home-medicines');
		var targetBottom = target.offset().top + target.outerHeight();

		var elem = jQuery('.recent-activity');
		elem.css('padding-bottom', '0px');
		var elemBottom = elem.offset().top + elem.outerHeight();

		if (windowWidth > 767 && elem.find('li').length > 3) {
			if (targetBottom > elemBottom) {
				elem.css('padding-bottom', (targetBottom - elemBottom) + 'px');
				elem.find('li:eq(3), li:eq(4)').removeClass('hidden');
			}
			else if (targetBottom < elemBottom) {
				if(elem.find('li').length > 3) {
					elem.find('li:eq(3), li:eq(4)').addClass('hidden');
					this.setActivityHeight();
				}
			}
			else {
				if(elem.find('li').length > 3) {
					elem.find('li:eq(3), li:eq(4)').removeClass('hidden');
					this.setActivityHeight();
				}
			}
		}
		else {
			if(elem.find('li').length > 3) {
				elem.css('padding-bottom', '45px');
				elem.find('li:eq(3), li:eq(4)').removeClass('hidden');
			}
		}
	},

	setPreferences: function() {
		$('#preference .js-preference-toggle li').on('click', function() {
			var therapyAreas = [];
			$('#preference .js-preference-toggle li.active').each(function(key, value) {
				therapyAreas.push($(this).data('id'));
			});

			$.ajax({
			  method: 'POST',
			  url: Drupal.settings.basePath + 'save-preference',
			  data: { 'therapyAreas': therapyAreas.join() }
			})
			.done(function( result ) {
				
			});
		});
	},

	setProfileUpdater: function() {
		$('.js-profile-update').on('click', function() {
			var _this = $(this);
			var arrFields = _this.data('field').split('|');
			var numError = 0;
			$.each(arrFields, function(key, field) {
				if(field == 'password-old')
					return true;

				var objElement = $('#' + field);

				objElement.attr('disabled', true);
				bolStatus = objForm.validate(objElement);
				numError += (bolStatus) ? 0 : 1;

				if(bolStatus)
					objElement.removeClass('error-field');
				else
					objElement.addClass('error-field');
				objElement.attr('disabled', false);

				if (numError == 0) {
					$.ajax({
					  method: 'POST',
					  url: Drupal.settings.basePath + 'save-profile-field',
					  data: { 'field': objElement.attr('name'), 'value': objElement.val() }
					})
					.done(function( result ) {	
						$('span[data-from="' + field + '"]').html(objElement.val());
						$('.js-profile-close[data-field="' + _this.data('field') + '"]').trigger('click');
						objElement.attr('disabled', false);
					});
				}
			});
		});
	},

	passwordScorer: function(pass) {
		// http://stackoverflow.com/questions/948172/password-strength-meter
	    var score = 0;
	    if (!pass)
	        return score;

	    // award every unique letter until 5 repetitions
	    var letters = new Object();
	    for (var i=0; i<pass.length; i++) {
	        letters[pass[i]] = (letters[pass[i]] || 0) + 1;
	        score += 5.0 / letters[pass[i]];
	    }

	    // bonus points for mixing it up
	    variationCount = 0;

	    // Number
	    if(pass.length >= 6) {
	    	variationCount += 1;
	    	$('p.pass-size').removeClass('error');
	    }
	    else {
	    	$('p.pass-size').addClass('error');
	    }

	    // Number
	    if(/\d/.test(pass)) {
	    	variationCount += 1;
	    	$('p.pass-number').removeClass('error');
	    }
	    else {
	    	$('p.pass-number').addClass('error');
	    }

	    // Lower case
	    if(/[a-z]/.test(pass)) {
	    	variationCount += 1;
	    	$('p.pass-lowercase').removeClass('error');
	    }
	    else {
	    	$('p.pass-lowercase').addClass('error');
	    }

	    // Upper case
	    if(/[A-Z]/.test(pass)) {
	    	variationCount += 1;
	    	$('p.pass-uppercase').removeClass('error');
	    }
	    else {
	    	$('p.pass-uppercase').addClass('error');
	    }

	    // Special Character
	    if(/\W/.test(pass)) {
	    	variationCount += 1;
	    	$('p.pass-char').removeClass('error');
	    }
	    else {
	    	$('p.pass-char').addClass('error');
	    }

	    score += (variationCount - 1) * 10;

	    score = parseInt(score);
	    note = '';
	    if (score > 80)
	        note = 'Strong';
	    else if (score > 60)
	        note = 'Good';
	    else if (score >= 0)
	        note = 'Weak';

	    $('.progress-password progress').attr('value', score);
	    $('.current-password-strength').html(note);
	},

	setRegistrationValidator: function(id) {
		$('#' + id + ' button[type="submit"]').on('click', function() {
			$('#' + id).trigger('submit');
		});

		$('#' + id).on('submit', function() {
			$('.validate-note').removeClass('active');
			var _this = $(this);
			var objError = '';
			var numError = 0;

			$.each(_this[0], function(key, elem) {
				var field = $(elem);
				var rules = field.data('rules');
				bolStatus = 1;
				if(rules) {
					bolStatus = objForm.validate(field);
					numError += (bolStatus) ? 0 : 1;
				}

				if(bolStatus) {
					field.removeClass('error-field');
				}
				else {
					field.addClass('error-field');
					objError = (objError == '') ? field : objError;
				}
			});
			
			if(numError > 0) {
				objError.trigger('focus');	
				return false;
			}
			else
				return true;
		});
	},

	setFormValidators: function() {
		$('.js-alpha').on('keydown input', function(){
			var regexp = /[^a-zA-Z ]/g;

			if ($(this).val().match(regexp))
				$(this).val($(this).val().replace(regexp, ''));
		});

		// $('.js-number').on('keydown input', function(){
		$('.js-number').on('change keyup keypress', function(){
			var regexp = /[^0-9\. ]/g;

			if ($(this).val().match(regexp))
				$(this).val($(this).val().replace(regexp, ''));
		});

		$('.phone-field').on('change keyup keypress', function(e) {

			// trim not numbers
		    var _v = $(this).val();
			var _fv = _v.replace(/[^0-9\.]/g, '');
			if (_v != _fv) {
		       $(this).val(_fv);
		    }

		    // trim 0 as first number
		    var _v = $(this).val();
		    if(_v.length == 1 && parseInt(_v) == 0) {
		    	$(this).val('');
		    }

		    // reformat
			var _v = $(this).val();
			var _nfv = _v.replace(/[^0-9\.]/g, '');
			var _fv = _v.replace(/[^0-9\.\(\) ]/g, '');
			var _code = _fv.substring(0, 2);
			var _tel = _fv.substring(2, _fv.length);
			var _new = (_nfv.length > 2) ? '(' + _code + ') ' : '';
			_new += (_nfv.length > 2) ? _tel : _nfv;
			_new = (_new.length > 9) ? _new.substring(0, 9) + ' ' + _new.substring(9) : _new;
			if(_new.length > 14) _new = _new.substring(0, 14);

			if (_v != _new) {
		       $(this).val(_new);
		    }

		});

	},

	setDisplayPicture: function() {
		$('select[name="field_titles"]').on('change', function() {
			if($(this).val() == 1) {
				$('#display-picture').removeClass('hidden');
			}
			else {
				$('#display-picture').addClass('hidden');
			}

			$('input[name="type"]').val($(this).val());
		});

		$('#profile').on('change', function() {
			$('#bulk-profile-form').trigger('submit');
  			$('.photo-c-holder img').attr('src', $('.photo-c-holder img').attr('loading-src'));
		});

		$('.photo-c-holder img').on('click', function() {
			$('#profile').trigger('click');
		});
	},

	setFAQAccordion: function() {
		$(".top-medicine-level").click(function(e) {
	        e.preventDefault();
	        $(this).closest('.top-medicine-level').siblings('.sub-medicines-list').slideToggle();
	        $(this).closest('.top-medicine-level').find('.medicines-btn a').toggleClass('open-icon'); 
	        $(this).closest('.top-medicine-level').find('.medicines-img').toggleClass('active-medicines-image');
	    });
	},

	overrideAutocomplete: function() {
		// Autocomplete
		$.ui.autocomplete.prototype._renderItem = function (ul, item) {
			var term = this.term;
			var first = ("group" in item)  ? 'first' : '';
			var innerHTML = '<div class="ui-autocomplete-fields ' + first + '">';
			if (item.fields) {
			  	$.each(item.fields, function(key, value) {
			    	var regex = new RegExp('(' + $.trim(term) + ')', 'gi');
			    	var output = value;
			    	if (value.indexOf('src=') == -1 && value.indexOf('href=') == -1) {
			      		output = value.replace(regex, "<span class='ui-autocomplete-field-term'>$1</span>");
			    	}
			    
			    	innerHTML += ('<div class="ui-autocomplete-field-' + key + '">' + output + '</div>');
			  	});
			} 
			else {
			  	innerHTML += ('<div class="ui-autocomplete-field">' + item.label + '</div>');
			}
			
			innerHTML += '</div>';

			// var group = '';
			// if ("group" in item) {
			// 	groupId = typeof(item.group.group_id) !== 'undefined' ? item.group.group_id : '';
			// 	groupName = typeof(item.group.group_name) !== 'undefined' ? item.group.group_name : '';
			//   	group += ('<div class="ui-autocomplete-field-group ' + groupId + '">' + groupName + '</div>');
			//   	$(group).appendTo(ul);
			// }

			var elem =  $("<li class=ui-menu-item-" + first + "></li>" )
			.append("<a>" + innerHTML + "</a>");   
			if (item.value == '') {
				elem = $("<li class='ui-state-disabled ui-menu-item-" + first + " ui-menu-item'>" + item.label + "</li>" );
			}
			elem.data("item.autocomplete", item).appendTo(ul);

			Drupal.attachBehaviors(elem);
			return elem;
		};

		Drupal.behaviors.search_autocomplete = {
		    attach: function(context) {
		      	if (Drupal.settings.search_autocomplete) {
			        $.each(Drupal.settings.search_autocomplete, function(key, value) {
			          $(Drupal.settings.search_autocomplete[key].selector).bind("mouseover", function() {
			             $(Drupal.settings.search_autocomplete[key].selector).addClass('ui-autocomplete-processed ui-theme-' + Drupal.settings.search_autocomplete[key].theme).autocomplete({
			            	 	minLength: Drupal.settings.search_autocomplete[key].minChars,
			            	 	source: function(request, response) {
			                  _url = Drupal.settings.search_autocomplete[key].datas;
			                  _url = _url.replace(/^\/|\/$/g, '');
			                  	Drupal.settings.search_autocomplete[key].datas = _url;
					              // External URL:
					              if (Drupal.settings.search_autocomplete[key].type == 'external') {

					                $.getJSON(Drupal.settings.search_autocomplete[key].datas, { q: encodeURIComponent(request.term) }, function (results) {
					                  // Only return the number of values set in the settings.
					                  if (results.length) {
					                  	results.slice(0, Drupal.settings.search_autocomplete[key].max_sug);
					                  }
					                  response(results);
					                });
					              }
					              // Internal URL:
					              else if (Drupal.settings.search_autocomplete[key].type == 'internal' || Drupal.settings.search_autocomplete[key].type == 'view') {
					                $.getJSON(Drupal.settings.search_autocomplete[key].datas, request, function (results) {
					                  // Only return the number of values set in the settings.
					                  if (results.length) {
				                	    results.slice(0, Drupal.settings.search_autocomplete[key].max_sug);
					                  }
					                  response(results);
					                });
					              }
					              // Static resources:
					              else if (Drupal.settings.search_autocomplete[key].type == 'static') {
					                var results = $.ui.autocomplete.filter(Drupal.settings.search_autocomplete[key].datas, request.term);
				                  if (results.length) {
				                    results.slice(0, Drupal.settings.search_autocomplete[key].max_sug);
				                  }
				                  response(results);
					              }
					            },
					            open: function(event, ui) {
					              $(".ui-autocomplete li.ui-menu-item:odd").addClass("ui-menu-item-odd");
					              $(".ui-autocomplete li.ui-menu-item:even").addClass("ui-menu-item-even");
					            },
					            select: function(event, ui) {
					            	console.log(ui.item.value);
					            	console.log($(this));
					            	var value = ui.item.value;
					            	var str = value.replace("&lt;sup&gt;","");
					            	str = str.replace("&lt;/sup&gt;","");
					            	console.log(str);
					              if (Drupal.settings.search_autocomplete[key].auto_redirect == 1 && ui.item.link) {
					              	$(this).val(123);
					                //document.location.href = ui.item.link;
					              } else if (Drupal.settings.search_autocomplete[key].auto_submit == 1 && ui.item.value) {
					              		console.log(ui.item.value);
					                  $(this).val(123);
					                  //$(this).closest("form").submit();
					              }
					            },
					            focus: function (event, ui) {
					              // if (typeof ui.item.group.group_id != "undefined") {
			                //         if (ui.item.group.group_id == 'no_results' || ui.item.group.group_id == 'all_results') {
			                //             event.preventDefault();
			                //         }
			                //       }  
					            },
					            appendTo: $(Drupal.settings.search_autocomplete[key].selector).parent(),
			            }).autocomplete({ 
			            	select : function(event, ui) {
				            	var value = ui.item.value;
				            	var str = value.replace("&lt;sup&gt;","");
				            	str = str.replace("&lt;/sup&gt;","");
				            	str = str.replace("&lt;sub&gt;","");
				            	str = str.replace("&lt;/sub&gt;","");

								if (Drupal.settings.search_autocomplete[key].auto_redirect == 1 && ui.item.link) {
								$(this).val(str);
									document.location.href = ui.item.link;
								} else if (Drupal.settings.search_autocomplete[key].auto_submit == 1 && ui.item.value) {
									$(this).val(str);
									$(this).closest("form").submit();
								}

				            	return false; 
				            }, 
				            focus : function(event, ui) {
				            	var value = ui.item.value;
				            	var str = value.replace("&lt;sup&gt;","");
				            	str = str.replace("&lt;/sup&gt;","");
				            	str = str.replace("&lt;sub&gt;","");
				            	str = str.replace("&lt;/sub&gt;","");
				            	$(this).val(str);
								// if (Drupal.settings.search_autocomplete[key].auto_redirect == 1 && ui.item.link) {
								// $(this).val(str);
								// 	document.location.href = ui.item.link;
								// } else if (Drupal.settings.search_autocomplete[key].auto_submit == 1 && ui.item.value) {
								// 	$(this).val(str);
								// 	$(this).closest("form").submit();
								// }

				            	return false; 
				            }
			        	}).attr("id", "ui-theme-" + Drupal.settings.search_autocomplete[key].theme);
			        	});
			          $(Drupal.settings.search_autocomplete[key].selector).trigger('mouseover');
			        });
		      	}
		    }
		};
	},

	videoPlayLogger: function() {
		if(objEvents.sendVideoLog == false) {
			objEvents.sendVideoLog = true;
			var base_url = Drupal.settings.astrazeneca_setting.base_url;
			$('a.btnplay').on('click', function(){
				if (typeof $(this).attr('nid') != "undefined") {
					$.ajax({
						'url': base_url + '/save_log/webinars',
						'method': 'GET',
						'data': {nid:$(this).attr('nid')},
						'success': function(msg){
							objEvents.sendVideoLog = false;
						}
					});
				}
			});
		}
	},

	passwordFieldReset: function() {
		$('input[type="password"]').val('');
	},

	dashboardExport: function() {
		$('.dashboard-export .export-btn').on('click', function(e) {
			e.preventDefault();
			$('#dashboard-export-form input[name="d"]').val($('#reportFilter #date-range').val());
			setTimeout(function() { $('#dashboard-export-form').submit(); }, 1000);
		});
	},

	init: function() {
		var _this = this;
		var body = $('body');
		_this.setActiveNav();
		_this.setFormValidators();
		_this.overrideAutocomplete();
		//_this.passwordFieldReset();

		if(body.hasClass('logged-in')) { // Logged In
			_this.videoPlayLogger();
			_this.dashboardExport();

			if (body.hasClass('page-profile')) {
				_this.setPreferences();
				_this.setProfileUpdater();

				$('#password-new').on('keydown keypress', function() {
					var strValue = $(this).val();
					_this.passwordScorer(strValue);
				});
			}
			else if (body.hasClass('page-clinical-papers')) {
				_this.setRegistrationValidator('form_request_clinical');
			}
			else if (body.hasClass('page-dashboard')) {
				$('#date-range').daterangepicker({
			    	format: 'MMM DD, YYYY',
			    	minDate : moment().subtract(3, 'months'),
			    	maxDate : moment(),
					startDate: moment().subtract(15, 'day'),
			        endDate: moment()
			    });

			    $('#date-range.right-date-range').daterangepicker({
			    	opens: 'left',
			    	format: 'MMM DD, YYYY',
			    	minDate : moment().subtract(3, 'months'),
			    	maxDate : moment(),
					startDate: moment().subtract(15, 'day'),
			        endDate: moment()

			    });

			    $('.range_inputs .applyBtn').click(function() {
					setTimeout("$('#reportFilter').submit()", 500);
				});
			}
			else if (body.hasClass('node-type-medicine')) {
				// _this.setFAQAccordion();
			}
		}
		else { // Logged Out
			if (body.hasClass('page-account-register') || body.hasClass('page-account-employee-register')) {
				_this.setRegistrationValidator('register_form');
				_this.setRegistrationValidator('bulk-profile-form');
				_this.setDisplayPicture();
			}
		}

		// General
		if (body.hasClass('page-adverse-event')) {
			$('#dp1, #dp2, #dp3').datetimepicker({'format' : 'DD/MM/YYYY'});
			_this.setRegistrationValidator('register_event_form');
		}
	},

	resizeInit: function() {
		var _this = this;
		var body = $('body');
	}
}

$(document).ready(function() {
	objTheme.init();
});

$(window).load(function() {
	objTheme.init();
});

$(window).resize(function() {
	objTheme.resizeInit();
});

function setImageSrc(uploaded_img,filename) {
	$('.photo-c-holder img').attr('src', uploaded_img);
	$('.photo-c-holder').addClass('round');
	$('form#register_form input[name="profile"]').val(filename); 
}