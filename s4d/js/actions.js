var $ = jQuery;
$(document).ready(function() {
  goToWebsite();
  gotoDownloadLink();
  eventRegister();
  toggleUpload();
  saveVideoLog();
  changeVariantQuantity();
  addMoreSecondAddress();
  validateStayuptoDate()
	initVideoModal();
	initYoutubeAPI();
	initVideo();

	var regexppostal = /[^0-9]/g;
	var regexp = /[^a-zA-Z]/g;
	var submit_result = new Array();
	// STREET
	$("input[name='address_text_user_input_street']").blur(function(){
		if ($(this).val().trim().length > 0) {
			$(this).removeClass('error');
			submit_result.push('true');
		} else {
			$(this).addClass('error');
			submit_result.push('false');
		}
	});

	// SUBURB
	$("input[name='address_text_user_input_city']").blur(function(){
		if (!$(this).val().match(regexp) && $(this).val().trim().length > 0) {
			$(this).val($(this).val().replace(regexp, ''));
			$(this).removeClass('error');
			submit_result.push('true');
		} else {
			$(this).addClass('error');
			submit_result.push('false');
		}
	});
	
	// STATE
	$("input[name='address_text_user_input_state']").blur(function(){
		if (!$(this).val().match(regexp) && $(this).val().trim().length > 0) {
			$(this).val($(this).val().replace(regexp, ''));
			$(this).removeClass('error');
			submit_result.push('true');
		} else {
			$(this).addClass('error');
			submit_result.push('false');
		}
	});

	// POSTAL
	$("input[name='address_text_user_input_postalcode']").blur(function(){
		if (!$(this).val().match(regexppostal) && $(this).val().trim().length > 0) {
			$(this).val($(this).val().replace(regexppostal, ''));
			$(this).removeClass('error');
			submit_result.push('true');
		} else {
			$(this).addClass('error');
			submit_result.push('false');
		}
	});
    
  //THIS WILL CLICK Resource TAB IF THE URL TYPE = RESOURCE
  var url = document.URL;
  if(url.indexOf("type=resources")>= 0){
    $('ul#sample-and-resources-tab a[aria-controls="resource"]').click();
  }

  if(url.indexOf("tab=webcasts")>= 0){
    $('ul#events-and-webcasts-tab a[aria-controls="webcasts"]').click();
  }
 
  
  $('select[name="filter-by-type"], select[name="filter-by-date"], select[name="filter-by-type-webcast"], select[name="sort-by-type"]').on('change',function(){
  	var current_tab = $('#events-and-webcasts-tab li.active a').attr('aria-controls');
	$(this).after('<input type="hidden" name="tab" value="'+current_tab+'"/>');
    $(this).parents('form').submit();
  });

   


  $('.postage').click(function(){
  	if($('.postage').is(':checked') ){
  		$('.mailing-address').html('Or Mailing Address<span>*</span>');
  	}
  	else{
  		$('.mailing-address').html('Or Mailing Address');
  	}
  });

  $('#order-confirmation form').submit(function(){
	$(this).find('select.custom-select').attr('disabled',false);
  });


  $('input[name="address_text_user_input_street"]').val('');
  $('input[name="address_text_user_input_city"]').val('');
  $('input[name="address_text_user_input_state"]').val('');
  $('input[name="address_text_user_input_postalcode"]').val('');

  $('input[name="address_text_user_input_street"], input[name="address_text_user_input_city"], input[name="address_text_user_input_state"], input[name="address_text_user_input_postalcode"]').blur(function(){
	if($(this).val() == $(this).attr('placeholder')){
	  $(this).val('');
	}
  });

  $('input[name="address_text"]').change(function(){
	  if ($(this).attr('id') != "address_text_x") {
		$('input#address_text_x').prop('checked', false);
	  }
  });

  $('input[name="address_text_user_input_street"], input[name="address_text_user_input_city"], input[name="address_text_user_input_state"], input[name="address_text_user_input_postalcode"]').keyup(function(){
	$('input[name="address_text"]').each(function(i,o){
	  if ($(o).attr('id') != "address_text_x") {
		$(o).prop('checked', false);
	  }
	});
	$('input#address_text_x').prop('checked', true);
  });

    $('input[name="firstname"]').each(function(i,o){
      $(o).val($(o).attr('value'));
    });
    $('input[name="lastname"]').each(function(i,o){
      $(o).val($(o).attr('value'));
    });
	
	$('select.select-therapy, select.select-condition-type').on('change',function(){
		if ($(this).hasClass('select-therapy')) {
			var ct = (typeof $('select.select-condition-type').val() != "object") ? $('select.select-condition-type').val() : '';
			window.location = window.location.pathname + '?t=' + $(this).val() + '&c=';
        } else if ($(this).hasClass('select-condition-type')) {
			var ta = (typeof $('select.select-therapy').val() != "object") ? $('select.select-therapy').val() : '';
			window.location = window.location.pathname + '?t=' + ta + '&c=' + $(this).val();
        }
	});
	$('.recieve-news .hotspot').on('click', function() {
	  $('#newsletter-trigger').trigger('click');
	});
	$('.recieve-news .close-hotspot').on('click', function() {
	  $('.recieve-news').hide();
	});
	

	// $('.login-content input[name="name"]').val('');
	// $('.login-content input[name="pass"]').attr('value','');
	// $('.login-content input[name="pass"]').prop('value','');
	// $('.login-content input[name="pass"]').val('');
	
	$('#preference li').on('click',function(){
	  var base_url = Drupal.settings.astrazeneca_setting.base_url;
	  var prefs = new Array();
	  $('#preference li.active').each(function(i,o){
		prefs.push($(o).attr('data-id'));
	  });
	  
	  var data = {tids:prefs}; 
	  $.post(base_url + '/doctor/ta/add', data, function(){
		console.log('Therapy Area saved ');
		console.log(data);
	  });
	});
	
	$('label[for="edit-pass-pass1--2"]').text('New Password');
	$('label[for="edit-pass-pass2--2"]').text('Re-type New');


	$('.recieve-news .close-hotspot').on('click', function(){
		var base_url = Drupal.settings.astrazeneca_setting.base_url;
		$.ajax({
		  'url': base_url + '/opt-in/close',
		  'method': 'GET',
		  'data': {},
		  'success': function(msg){
			console.log('close opt-in popup');
		  }
		});
	});
});

$(window).bind("load", function() {
	adjustAnchor();
	var tm = setTimeout(function(){
	  clearTimeout(tm);
	},1000);
	// $('.login-content input[name="name"]').val('');
	// $('.login-content input[name="pass"]').attr('value','');
	// $('.login-content input[name="pass"]').prop('value','');
	// $('.login-content input[name="pass"]').val('');
});
function goToWebsite() {
	$('a[data-target="#dialog-website"], a[data-target="#dialog-professional-societies"], a[data-target="#dialog-event-offset-link-pdf"]').click(function(e){
		e.preventDefault();
		_target = $(this).attr('data-target');
		_url = ($(this).attr('href') != '') ? $(this).attr('href') : $(this).attr('data-href');
		if (_target == "#dialog-website") {
			console.log($(this).attr('href'));
			console.log(_url);
			if(_url.indexOf("staging.azhealth.com.au") > -1 || _url.indexOf("staging.simply4doctors.com.au") > -1 || _url.indexOf("http") < 0) {
				$(this).removeAttr('data-toggle').removeAttr('data-target').removeAttr('target');
				window.location = _url;
			}
			else {
				$(this).attr('href', '').attr('data-href', _url);
			}
		}
		else {
			window.location = _url;
		}

		$(_target + ' a').attr('href', _url);

		$('#dialog-website a.btn-secondary').click(function(){
		  $('a[data-dismiss="modal"]').trigger('click');
		});

		if (typeof $(this).attr('main-thumbnail') != "undefined") {
		  if ($(this).attr('main-thumbnail') != "") {
			$('img.main-thumbnail').attr('src',$(this).attr('main-thumbnail'));
		  }

		}
		if ($(this).attr('data-target') == "#dialog-event-offset-link-pdf") {
		  $($(this).attr('data-target') + ' a').attr('href',  '/download-pdf?download_file=' + $(this).attr('href') + '&download_type=Events');

		}
		//$($(this).attr('data-target') +' .modal-body .destination').html($(this).attr('href'));
	});
}

function gotoDownloadLink() {
	$('select.data-switcher').change(function(){
		var data_link = $(this).find('option:selected').attr('data');
		var data2 = (typeof data_link != "undefined") ? $(this).find('option:selected').attr('data2') : '0';
		if (typeof data_link != "undefined") {
			$(this).parents('.row').find('#download-link[data-id="' + $(this).attr('resource-id') + '"]').attr('href', Drupal.settings.basePath + 'download-pdf?download_file=' + data_link + '&download_type=Resource&nid=' + data2 );
            console.log(data_link);
        }
        
		var data_link2 = $(this).find('option:selected').attr('data2');
		if (typeof data_link2 != "undefined") {
          if (data_link2 != '') {
            $(this).parents('.row').find('#download-link2[data-id="' + $(this).attr('resource-id') + '"]').attr('href', data_link2).show();
          }else{
			$(this).parents('.row').find('#download-link2[data-id="' + $(this).attr('resource-id') + '"]').attr('href', data_link2).hide();
          }
		}
	});


	$('a#download-link').each(function(i,o){
        var data_link = $('select.data-switcher[resource-id="' + $(o).attr('data-id') + '"]').find('option:selected').attr('data');
		var data_title = $(this).attr('resource-title');
		var data_id = $(this).attr('data-id') ;
        $(o).attr('href', Drupal.settings.basePath + 'download-pdf?download_file=' + data_link + '&download_type=Resource&title=' + data_title + '&nid=' + data_id ); 
    });
    
	$('a#download-link2').each(function(i,o){
    	var data_link2 = $('select.data-switcher[resource-id="' + $(o).attr('data-id') + '"]').find('option:selected').attr('data2');
		if (typeof data_link2 != "undefined") {
          if (data_link2 != '') {
            $(o).attr('href', data_link2).show();
          }else{
			$(o).attr('href', data_link2).hide();
          }
		}
    });

}

function adjustAnchor() {
  try{
	var nid = Drupal.settings.js_setting.nid;
	if (typeof nid != null) {
	  if (nid != 0) {
		var background_color = $('div#resource-'+nid).css('background-color');

		$( "html, body" ).animate({
		  scrollTop: ($('div#resource-'+nid).offset().top - 300)
		}, 500, function(){
		  $('div#resource-'+nid).css({'border': '1px solid #830051'});
		});

	  }
	}
  }catch(e){
	// @Nothing to do
  }
}

function addVariant(elem) {
  var time = new Date().getTime();
  var parent_row = $(elem).parents('#samples-item-row');
  $('#sample-add-variant .modal-body div.error-msg').html('');
  $('#sample-add-variant .modal-title').html('Add variant for ' + parent_row.find('h4').text());
  select_box = '<div class="custom-select-wrapper"><div class="custom-select-display">'+parent_row.find('#sample-sizes-select').val()+'</div>';
  select_box += '<select class="custom-select form-select" id="sample-sizes-select" name="sample-size[]">';
  select_box += parent_row.find('#sample-sizes-select').html();
  select_box += '</select></div>';
  $('#sample-add-variant .modal-body p').html( select_box );

  initCustomFormFields();

  $('#save-variant').bind('click',function(e){
	var modal_selected_value = $('#sample-add-variant .modal-body p select').val();
	 if ($('div[variant-size="'+modal_selected_value +'"][row-entity-id="'+ $(elem).attr('variant-entity-id') +'"]').length > 0 ) {
	  $('#sample-add-variant .modal-body div.error-msg').html('<span style="color:red;">Selected variant is already in the cart.</span>');
	 }else{
		var next_row = parent_row.html();
		//var row = $('div[variant-size="' + modal_selected_value+'"][row-entity-id="' + $(elem).attr('variant-entity-id') + '"]');

		$('#sample-add-variant .modal-body div.error-msg').html('');
		parent_row.after('<div class="row row-'+time+'" id="samples-item-row" variant-size="'+modal_selected_value+'" row-entity-id="' + $(elem).attr('variant-entity-id') + '">' + next_row + '</div>').promise().done(function(){
		  var samples_row = $('div[variant-size="' + modal_selected_value+'"][row-entity-id="' + $(elem).attr('variant-entity-id') + '"]');
		  samples_row.find('.remove-order').attr('size', modal_selected_value);
		  samples_row.find('#sample-sizes-select').parents('.custom-select-wrapper').find('.custom-select-display').text(modal_selected_value);
		  samples_row.find('#sample-sizes-select').val(modal_selected_value);
		  samples_row.find('.remove-order').removeClass('remove-order').addClass('remove-order-customize');
		});
		$('button[data-dismiss="modal"]').trigger('click');
		$('.remove-order-customize').bind('click',function(e){
		  $(this).parents('div#samples-item-row').remove();
		  e.preventDefault();
		});
	 }


	$('.custom-select-display').each(function(i,o){
	  if($(o).text().trim() == ""){
		$(o).parents('div#samples-item-row').remove();
	  }
	});
	initCustomFormFields();
	e.preventDefault();
  });
}

function initCustomFormFields() {
  $('select.custom-select').each(function() {
	  $(this).change(function() {
		  $(this).siblings('.custom-select-display').text( $(this).find('option:selected').text() );
	  });
	  $(this).keyup(function() {
		  $(this).siblings('.custom-select-display').text( $(this).find('option:selected').text() );
	  });
	  $(this).change();
  });
}


function addVariantV2(elem) {
  var time = new Date().getTime()
  var parent_row = $(elem).parents('#samples-item-row');
  var select_box = parent_row.find('select#sample-sizes-select');
  var modal_selected_value = select_box.val();

  var next_row = parent_row.html();
  $(elem).hide();
  parent_row.after('<div class="row row-'+time+'" id="samples-item-row" variant-size="'+modal_selected_value+'" row-entity-id="' + $(elem).attr('variant-entity-id') + '">' + next_row + '</div>').promise().done(function(){
	var samples_row = $('div[variant-size="' + modal_selected_value+'"][row-entity-id="' + $(elem).attr('variant-entity-id') + '"]');
	samples_row.find('.remove-order').attr('size', modal_selected_value);
	samples_row.find('#sample-sizes-select').parents('.custom-select-wrapper').find('.custom-select-display').text(modal_selected_value);
	samples_row.find('#sample-sizes-select').val(modal_selected_value);
	samples_row.find('.remove-order').removeClass('remove-order').addClass('remove-order-customize');

	samples_row.find('.qty-container').html('');
	samples_row.find('a#add-variant').hide();
	samples_row.find('#sample-sizes-select').find('option[value="'+modal_selected_value+'"]').remove();
	console.log(samples_row.find('#sample-sizes-select').find('option').length);

	samples_row.find('#sample-sizes-select').on('change',function(){
	  var max_order =  $(this).find('option:selected').attr('data');
	  var html_options = '';
	  samples_row.find('.qty-container').html('');
	  if (max_order == 1){
		html_options += '<h5 class="hidden-md hidden-lg">Qty</h5>';
		html_options += '<div class="single-quantity">1</div>';
		html_options += '<input type="hidden" name="sample-quantity[]" value="1">';
		samples_row.find('.qty-container').html(html_options);
	  }else{
		html_options += '<h5 class="hidden-md hidden-lg">Qty</h5>';
		html_options += '<div class="custom-select-wrapper">';
		html_options += '<div class="custom-select-display">1</div>';
		html_options += '<select class="custom-select quantity-select form-select" name="sample-quantity[]">';
		for (var i=1;i<=max_order;i++){
		  html_options += '<option value="'+i+'">'+i+'</option>';
		}
		html_options += '</select>';
		html_options += '</div>';
		samples_row.find('.qty-container').html(html_options);
		samples_row.find('select.custom-select').change(function() {
			$(this).siblings('.custom-select-display').text( $(this).find('option:selected').text() );
		});
	  }
	});
	samples_row.find('#sample-sizes-select').parents('.custom-select-wrapper').after(samples_row.find('#sample-sizes-select').val());
	samples_row.find('#sample-sizes-select').parents('.custom-select-wrapper').hide();

  });


  $('div.row-'+time).find('#sample-sizes-select').attr('disabled',false);
  $('.remove-order-customize').bind('click',function(e){
	$(this).parents('div#samples-item-row').remove();
	e.preventDefault();
  });

  initCustomFormFields();
}

function changeVariantQuantity(){
  $('select#sample-sizes-select').on('change',function(){
	var max_order =  $(this).find('option:selected').attr('data');
	var html_options = '';
	$(this).find('.qty-container').html('');
	if (max_order == 1){
	  html_options += '<h5 class="hidden-md hidden-lg">Qty</h5>';
	  html_options += '<div class="single-quantity">1</div>';
	  html_options += '<input type="hidden" name="sample-quantity[]" value="1">';
	  $(this).parents('.order-resource-btn').find('.qty-container').html(html_options);
	}else{
	  html_options += '<h5 class="hidden-md hidden-lg">Qty</h5>';
	  html_options += '<div class="custom-select-wrapper">';
	  html_options += '<div class="custom-select-display">1</div>';
	  html_options += '<select class="custom-select quantity-select form-select" name="sample-quantity[]">';
	  for (var i=1;i<=max_order;i++){
		html_options += '<option value="'+i+'">'+i+'</option>';
	  }
	  html_options += '</select>';
	  html_options += '</div>';
	  $(this).parents('.order-resource-btn').find('.qty-container').html(html_options);
	  $(this).parents('.order-resource-btn').find('select.custom-select').change(function() {
		  $(this).siblings('.custom-select-display').text( $(this).find('option:selected').text() );
	  });
	}
  });
}

function eventRegister(){
	// Event Date time default selection
	$('select#location-id').each(function(i,o){
	  var data_val = $(o).find('option[value="'+$(o).val()+'"]').attr('data');
	  if (data_val != "") {
		$(o).parents('form').find('#event-date-time').html(data_val);
		$(o).parents('form').find('.additional-fields-container').hide();
		$(o).parents('form').find('#event-location-' + $(o).val()).show();
	  }
	});
 	$('input#location-id').each(function(i,o){
	  if ($(o).val() != "") {
		$(o).parents('form').find('.additional-fields-container').hide();
		$(o).parents('form').find('#event-location-' + $(o).val()).show();
	  }
	});
  selectEventLocation();
}

function selectEventLocation() {
  $('select#location-id').on('change',function(){
	// Event Date time selection
	var data_val = $(this).find('option[value="'+$(this).val()+'"]').attr('data');
	if (data_val != "") {
	  $(this).parents('form').find('#event-date-time').html(data_val);
	  $(this).parents('form').find('.additional-fields-container').hide();
	  $(this).parents('form').find('#event-location-' + $(this).val()).show();
	}
  });
}

function toggleUpload() {
	var ep = $('.registration-form input[name="field_email_address"]').attr('placeholder');
	$('.upload-field').removeClass('animated fadeInDown active').hide();
	$('#registration-user-type').on('change', function() {
		t = $(this).val();
		if(t == 1){
		  $('.upload-field').show().addClass('animated fadeInDown active');
		  $('.registration-form input[name="field_email_address"]').attr('placeholder', ep);
		}
		else{
		  $('.upload-field').removeClass('animated fadeInDown active').hide();
		  $('.registration-form input[name="field_email_address"]').attr('placeholder', 'Please enter your @astrazeneca.com email address');
		}
	});
}

function saveVideoLog() {
  /*var base_url = Drupal.settings.astrazeneca_setting.base_url;
  $('div#watch-video').click(function(){
	if (typeof $(this).find('a').attr('nid') != "undefined") {
	  $.ajax({
		'url': base_url + '/save_log/webinars',
		'method': 'GET',
		'data': {nid:$(this).find('a').attr('nid')},
		'success': function(msg){
		  console.log(msg);
		}
	  });
	}
  });*/
}

function getDownFilename(downloadLink){
  var filename = '';
  try {
	var link = downloadLink.split('=');
	if (typeof link[1] != "undefined") {
	  var link2 = link[1].split('&');
	  if (typeof link2[0] != "undefined") {
		var link3 = link2[0].split('/');
		var link3count = link3.length;
		if (typeof link3[link3count-1] != "undefined") {
		  filename = link3[link3count-1];
		}
	  }
	}
  } catch(e) {

  }
  return filename;
}

function validateShippingAddress() {
  var submit_result = new Array();
  if (typeof $('input[name="address_text"]:checked').val() == 'undefined') {
	submit_result.push('false');
  }
  if ('custom_address' == $('input[name="address_text"]:checked').val()) {

  	//STREET
	if ($('input[name="address_text_user_input_street"]').val().trim().length > 0) {
	  $('input[name="address_text_user_input_street"]').removeClass('error');
	  submit_result.push('true');
	}else{
	  $('input[name="address_text_user_input_street"]').addClass('error');
	  submit_result.push('false');
	}

	$("input[name='address_text_user_input_street']").blur(function(){
		if ($(this).val().trim().length > 0) {
			$(this).removeClass('error');
			submit_result.push('true');
		} else {
			$(this).addClass('error');
			submit_result.push('false');
		}
	});

	// SUBURB
	var regexp = /[^a-zA-Z\s]/g;

	if (!$("input[name='address_text_user_input_city']").val().match(regexp) && $("input[name='address_text_user_input_city']").val().trim().length > 0) {
		$("input[name='address_text_user_input_city']").val($("input[name='address_text_user_input_city']").val().replace(regexp, ''));
		$("input[name='address_text_user_input_city']").removeClass('error');
		submit_result.push('true');
	} else {
		$("input[name='address_text_user_input_city']").addClass('error');
		submit_result.push('false');
	}

	$("input[name='address_text_user_input_city']").blur(function(){
		if (!$(this).val().match(regexp) && $(this).val().trim().length > 0) {
			$(this).val($(this).val().replace(regexp, ''));
			$(this).removeClass('error');
			submit_result.push('true');
		} else {
			$(this).addClass('error');
			submit_result.push('false');
		}
	});
	
	// STATE
	if (!$("input[name='address_text_user_input_state']").val().match(regexp) && $("input[name='address_text_user_input_state']").val().trim().length > 0) {
		$("input[name='address_text_user_input_state']").val($("input[name='address_text_user_input_state']").val().replace(regexp, ''));
		$("input[name='address_text_user_input_state']").removeClass('error');
		submit_result.push('true');
	} else {
		$("input[name='address_text_user_input_state']").addClass('error');
		submit_result.push('false');
	}

	$("input[name='address_text_user_input_state']").blur(function(){
		if (!$(this).val().match(regexp) && $(this).val().trim().length > 0) {
			$(this).val($(this).val().replace(regexp, ''));
			$(this).removeClass('error');
			submit_result.push('true');
		} else {
			$(this).addClass('error');
			submit_result.push('false');
		}
	});

	// POSTAL
	var regexppostal = /[^0-9]/g;
	if (!$("input[name='address_text_user_input_postalcode']").val().match(regexppostal) && $("input[name='address_text_user_input_postalcode']").val().trim().length > 0) {
		$("input[name='address_text_user_input_postalcode']").val($("input[name='address_text_user_input_postalcode']").val().replace(regexppostal, ''));
		$("input[name='address_text_user_input_postalcode']").removeClass('error');
		submit_result.push('true');
	} else {
		$("input[name='address_text_user_input_postalcode']").addClass('error');
		submit_result.push('false');
	}

	$("input[name='address_text_user_input_postalcode']").blur(function(){
		if (!$(this).val().match(regexppostal) && $(this).val().trim().length > 0) {
			$(this).val($(this).val().replace(regexppostal, ''));
			$(this).removeClass('error');
			submit_result.push('true');
		} else {
			$(this).addClass('error');
			submit_result.push('false');
		}
	});
  }else{
	submit_result.push('true');
  }
  if (submit_result.indexOf('false') >= 0) {
	return false;
  }else{
	return true;
  }

}




function initYoutubeAPI() {
    tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
}


function initVideo() { 
	$(".btnplay").on('click',function(e) {  	 
		e.preventDefault();
		if ($(this).attr('data-kaltura') != "") {
			KWidget.embed({
				'targetId': 'video-holder',
				'wid': '_432521',
				'uiconf_id' : '34612392',
				'entry_id' : $(this).attr('data-kaltura'),
				'flashvars':{ // flashvars allows you to set runtime uiVar configuration overrides. 
					'autoPlay': true
				},
				'params':{ // params allows you to set flash embed params such as wmode, allowFullScreen etc
					'wmode': 'transparent' 
				}
			});
			$(".modal-dialog .video-holder").css('height','490px');
        }else{
			var videourl = $(this).attr("data-videourl");
			playerType = ( videourl.indexOf('youtube')>0 ) ? 'youtube' : ( videourl.indexOf('vimeo')>0 ) ? 'vimeo' : '';
			playerType = ( videourl.indexOf('youtube.com/playlist?list=')>0 ) ? 'youtube_playlist' : playerType;

			if ( playerType == 'vimeo' ) {  
				var videobaseurl = videourl.split('?')[0];
				var videobasesplit = videobaseurl.split('/');
				var videoid = videobasesplit[ videobasesplit.length-1 ];
				//console.log(videoid);
				var iframe_html = '<iframe src="//player.vimeo.com/video/'+videoid+'?api=1&player_id=playerVideo&autoplay=1" id="playerVideo" width="100%" height="490" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
				if ($(".modal-dialog .video-holder").length > 0) {
                    $(".modal-dialog .video-holder").html(iframe_html);
                }else{
					$(".modal-dialog").html('<div class="video-holder">'+iframe_html+'</div>');
				} 
				
				iframeVimeo = $('#playerVideo')[0];
				playerVimeo = $f(iframeVimeo);
	
				playerVimeo.addEvent('ready', function(e) {
					//onPlayerReadyVideo(); 
					playerVimeo.addEvent('finish', function(e) {
						//stopAllVideo();
					});
				});
			}
	
			if ( playerType == 'youtube' ) {
	
				var videobaseurl = videourl.split('?')[0];
				var videobasesplit = videobaseurl.split('/');
				var videoid = videobasesplit[ videobasesplit.length-1 ];
				//console.log(videoid);
				$(".modal-dialog .video-holder").append("<div id='playerYoutube'></div>");
				playerYoutube = new YT.Player('playerYoutube', {
					height: 'auto',
					width: '100%',
					playerVars: { 'controls':0, 'showinfo':0, 'cc_load_policy':0, 'rel':0, 'autoplay':1},
					videoId: videoid,
					events: {
						'onStateChange': onPlayerStateChange
					}
				});
			}
	
			if ( playerType == 'youtube_playlist' ) {
				//console.log(videourl);
				var videobaseurl = videourl.split('?')[1];
				var videobasesplit = videobaseurl.split('list=');
				var videoid = videobasesplit[1];
				console.log(videoid);
				$(".modal-dialog .video-holder").append("<div id='playerYoutube'></div>");
				playerYoutube = new YT.Player('playerYoutube', {
					height: 'auto',
					width: '100%',
					playerVars: { listType:'playlist', list: videoid, 'controls':1, 'showinfo':0, 'cc_load_policy':0, 'rel':0, 'autoplay':1}
				});
			}
		}
	}); 
}


function stopAllVideo() {
	$(".modal-dialog .video-holder").html('');
}

function initVideoModal() {
	$('.modal-video').on('hidden.bs.modal', function (e) {
		stopAllVideo(); 
	})
}

function addMoreSecondAddress() {
  $('a#add-more-secondary-address').on('click',function(e){
	e.preventDefault(); 
	var fieldsets = $('.second-addresses-wrapper').find('fieldset'); 

	// validate if all fields are not empty
	var isLastFieldsetValid = true;
	fieldsets.last().find('input[type="text"]').each(function() {
		var inputElem = $(this);
		var inputValue = $(this).val();
		if(inputValue.trim() == '') {
			isLastFieldsetValid = false;
			inputElem.addClass('has-error');
		} else {
			inputElem.removeClass('has-error');
		}
	});

	if(isLastFieldsetValid) {

		fieldsets.last().after( '<fieldset class="collapsible form-wrapper collapse-processed" id="edit-contact-'+fieldsets.length+'">' + fieldsets.last().html() + '</div>');
		
		var new_fieldsets = $('.second-addresses-wrapper').find('fieldset');
		new_fieldsets.last().find('.form-item-street').find('input').attr('name', 'secondary_address['+(new_fieldsets.length - 1)+'][street]').val('');
		new_fieldsets.last().find('.form-item-city').find('input').attr('name', 'secondary_address['+(new_fieldsets.length - 1)+'][city]').val('');
		new_fieldsets.last().find('.form-item-state').find('input').attr('name', 'secondary_address['+(new_fieldsets.length - 1)+'][state]').val('');
		new_fieldsets.last().find('.form-item-postal-code').find('input').attr('name', 'secondary_address['+(new_fieldsets.length - 1)+'][postal_code]').val('');  

	} else {
		
	}
  }); 
}


function removeSecondAddress(elem) {
	if ($('.second-addresses-wrapper').find('fieldset').length > 1) {
	  $(elem).parents('fieldset').fadeOut(1000, function(){
		$(this).remove();
	  });
    } 
	return false;
}


function validateStayuptoDate() {
	$('form#astrazeneca-profile-form-edit-stay-uptodate').on('submit',function(e){ 
	  if(!$(this).find('input#edit-agree').is(':checked')){
		e.preventDefault();
		$(this).find('label[for="edit-agree"]').addClass('error');
		return false;
	  }else{
		$(this).submit();
	  }
	});
}

function anchorResourceItem(nid) {
	$( "html, body" ).animate({
		scrollTop: ($('li#resource-'+nid).offset().top - 300)
	}, 500, function(){
		$('li#resource-'+nid).css({'border': '1px solid #830051'});
	});
}