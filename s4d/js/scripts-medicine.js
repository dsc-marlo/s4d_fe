var $ = jQuery;

var isFromSubNav = false;

$(document).ready(function() {

	$('.medicine-single').addClass('show');

	// medicine single
	initCardio();
	resizeCardio();
	scrollCardio();

	// images
	doCoverImage();
	doCoverHeight();

	initHypertension();
	scrollHypertension();

	// lazyload
	$("img.lazyload").lazyload({
		effect: "fadeIn"
	});

	// forms placeholder
	/*$('[placeholder]').focus(function() {
	    var i = $(this);
	    if (i.val() == i.attr('placeholder')) {
	        i.val('').removeClass('placeholder');
	        if (i.hasClass('password')) {
	            i.removeClass('password');
	            this.type = 'password';
	        }
	    }
	}).blur(function() {
	    var i = $(this);
	    if (i.val() == '' || i.val() == i.attr('placeholder')) {
	        if (this.type == 'password') {
	            i.addClass('password');
	            this.type = 'text';
	        }
	        i.addClass('placeholder').val(i.attr('placeholder'));
	    }
	}).blur().parents('form').submit(function() {
	    $(this).find('[placeholder]').each(function() {
	        var i = $(this);
	        if (i.val() == i.attr('placeholder'))
	            i.val('');
	        i.removeClass('placeholder');
	    })
	});*/

	// submenu click

	$('#sub-menu-links .anchor').click(function(e) {
	    e.preventDefault();

	    isFromSubNav = true;
	    $('#sub-menu-links > li.active').removeClass('active');
	    $(this).parent().addClass('active');

	    var el = jQuery(this).attr('href');
	    var elWrapped = jQuery(el);
	    scrollToDiv(elWrapped, 0);
	    return false;
	});

	// medicine single - alternate section background
	if($('#main-wrapper.medicine-single').length) {
		$('#main-wrapper.medicine-single #cardio-hero ~ .section').not('.related-medicines-mob').each(function(index) {
			if(index % 2 == 1) {
				$(this).addClass('bg-gray');
			}
		});
	}

	
});

$(window).load(function() {
	resizeCardio();
	doCoverImage();
	doCoverHeight();

	resizePortfolio();
});

$(window).resize(function() {
	resizeCardio();
	doCoverImage();
	doCoverHeight();
	scrollHypertension();
	scrollCardio();

	resizePortfolio();

});

$(window).scroll(function(){
	scrollHome();
	scrollMedicine();
	scrollHypertension();
	scrollCardio();
});

function initCardio() {
	$('.cardio-hero-container .left-content .btn-rm').click(function(e){
		e.preventDefault();
		$('.cardio-hero-container .left-content .btn-rm').addClass('hiding');
		$('#read-more-content').slideDown(300);
	});

	$('#read-more-content .close-x').click(function(e){
		e.preventDefault();
		$('.cardio-hero-container .left-content .btn-rm').removeClass('hiding');
		$('#read-more-content').slideUp(300);
	});
}

function resizeCardio() {

	$('.cardio-hero-container .left-content, #cardio-hero-banner').css({'height' : 'auto'});
	$('#cardio-hero').css({'margin-top':0});
	var chH = $('#cardio-hero').height();
	$('.cardio-hero-container .left-content, #cardio-hero-banner').css({'height' : chH });
	$('#cardio-hero').css({'margin-top':-chH});

}

function scrollCardio() {

	if ( $('html').hasClass('desktop') ) {
		var sT = $(window).scrollTop();
		$('.cardio-slider-x').css('marginTop',sT*0.75);
	}

	if(window.innerWidth > 1024 ) {
		var sT = $(window).scrollTop();

		$('.medicine-single #cardio-hero-banner img.cardio-slider').css({
			'-webkit-transform': 'translate(-50%, ' + eval(sT * 0.75) + 'px)',
			'-ms-transform': 'translate(-50%, ' + eval(sT * 0.75) + 'px)',
			'-o-transform': 'translate(-50%, ' + eval(sT * 0.75) + 'px)',
			'transform': 'translate(-50%, ' + eval(sT * 0.75) + 'px)'
		});

	}
}

function doCoverImage() {

	// $('.hero-slider img, .article-video .article-thumb img, .hero-medicine-banner img, img.cardio-slider, img.cardio-slider-mobile').each(function() {
	$('.hero-slider img, .article-video .article-thumb img, .hero-medicine-banner img, img.cardio-slider-mobile').each(function() {
		coverImage( $(this) );
	});

}

function doCoverHeight() {

	coverHeight( $('#home-coreareas a') );
	coverHeight( $('#home-events .article-video, #home-events .article-event') );
	coverHeight( $('#home-websites .article-website') );
	coverHeight( $('#modal-login .login-content') );
	coverHeight( $('.gc-content .pwrapper') );
	coverHeight( $('.gc-content .h3wrapper') );

}

function coverImage( image ) {

	var imgObj = image;
	var iW = imgObj.attr('width');//width(); //width of image ratio
	var iH = imgObj.attr('height');//.height(); //height of image ratio

	imgObj.width(0).height(0);
	
	var imgContainer = image.parent();
	var cW = imgContainer.width(); //width of container or browser
	var cH = imgContainer.height(); //height of container or browser
	//console.log(iW,iH,cW,cH);

	if ( cH > 1 ) {
		var cP = cW/cH; //ratio of container or browser
		var iP = iW/iH; //ratio of image

		if ( iP > cP ) { //if image ratio is more than container ratio (if image width is more than container width)
			iH = cH; //set image height from container height
			iW = cH * iP; //set image width using container height and image ratio
			imgObj.css({
				'margin-top': 0,
				'margin-left': Math.ceil((cW-iW)/2),
				'width': Math.ceil(iW),
				'height': Math.ceil(iH)
			}); //center the image and set dimensions
		} else { //if image ratio is less than container ratio (if image height is more than container height)
			iW = cW; //set image width from container width
			iH = cW / iP; //set image height from container width and ratio
			imgObj.css({
				'margin-top': Math.ceil((cH-iH)/2),
				'margin-left': 0,
				'width': Math.ceil(iW),
				'height': Math.ceil(iH)
			}); //center the image and set dimensions
		}
	} else {
		imgObj.css({
			'margin-top': 0,
			'margin-left': 0,
			'width': 'auto',
			'height': 'auto'
		});
	}
}

function coverHeight( elems ) {
	var maxHeight = 0;
	$(elems).each(function() {
		$(this).css({ 'min-height' : '0' });
		var oH = $(this).outerHeight(false);
		maxHeight = (maxHeight > oH) ? maxHeight : oH;
	});
	$(elems).each(function() {
		$(this).css({ 'min-height' : maxHeight });
	});

}


function scrollHome() {

	if ( $('html').hasClass('desktop') ) {
		var sT = $(window).scrollTop();
		$('.hero-slider').css('top',sT*0.75);
		if ( $('.hero-container').css('position') == 'relative' ) {
			$('.hero-container').css('top',sT*0.4);
		} else {
			$('.hero-container').css('top','auto');
		}
	}

}

function scrollMedicine() {

	if ( $('html').hasClass('desktop') ) {
		var sT = $(window).scrollTop();
		$('.hero-medicine-banner').css('top',sT*0.5);		
	}

}

function scrollHypertension() {
	if ( $('.sub-nav').size() > 0 ) {
		// if ( $(window).scrollTop() > $('.sub-nav').offset().top - $('.header-nav1').height() - $('.header-nav2').height() ) {
		if ( $(window).scrollTop() > $('.sub-nav').offset().top - $('header').outerHeight() ) {
			$('.sub-nav .sub-nav-sticky').addClass('fixed');
		} else {
			$('.sub-nav .sub-nav-sticky').removeClass('fixed');
		}

		if(!isFromSubNav) {
			$('.sub-nav a.anchor').each(function(i) {
				var getPos = $( $(this).attr('href') ).offset().top - $(window).scrollTop();

				// if ( getPos < $(window).height()*2/3 ) {
				if ( getPos < $('header').height() + $('.sub-nav').height() ) { // if this reached top
					$('.sub-nav a.anchor').parent().removeClass('active');
					$(this).parent().addClass('active');
				} else if ( $(window).scrollTop() >= $(document).height() - $(window).height() ) { // if the scroll reached bottom

				} else {
					$(this).parent().removeClass('active');
				}
			});
		}
	}
}

function initHypertension() {
	$('.mobile-accordion').click(function(){
		$(this).next('.section').stop(true,false).slideToggle();
		$(this).children('h6').stop(true,false).toggleClass('open');
		$(window).resize();
	});
}

function scrollToDiv(element, offset){
    var offsetTop = element.offset().top - offset;
    $('body,html').animate({
        scrollTop: offsetTop - $('header').height() - $('.sub-nav').height()
    }, 500, function() {
    	isFromSubNav = false;
    });
}

function resizePortfolio() {
	var related = $('#related-medicines');
	if(window.innerWidth > 991) {
		var relatedLeft = $('#product-info-left');

		related.height('auto');
		if(related.outerHeight() < relatedLeft.outerHeight()) {
			related.outerHeight(relatedLeft.outerHeight());
		}
	} else {
		related.outerHeight('auto');
	}
}

