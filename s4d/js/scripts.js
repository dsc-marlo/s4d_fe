var $ = jQuery;

$(document).ready(function() {

	// FUNCTION
	parallax();
	initCustomForm();

	// page height
	resizeHome();

	// home tiles
	homeTiles();

	// re-pad body
	repadBody();

	// TOGGLE NEED ANYTHING
	$('.close-toggle').click(function(){
		$(this).parents().removeClass('active-toggle');
	});

	$('.med-list-wrap').click(function(e){
		if($(this).closest('li').hasClass('active')) {
			$(this).closest('li').removeClass('active');
			$(this).closest('li').find('.inner-content').stop(true,false).slideUp();
		} else {
			// $('.medicine-list > ul > li.active').removeClass('active');
			$(this).closest('li').addClass('active');
			$(this).closest('li').find('.inner-content').stop(true,false).slideDown();
		}

		return false;
	});
	
	$('.header-anything').click(function(){
		$(this).parent().parent().addClass('active-toggle');
	});

	// RECIEVE NEWS
	$('.recieve-news').click(function(){
		//$(this).fadeOut();
	});


	// MOBILE NAVIGATION
	 $('.mobile-nav .navigation-hamburger').click(function(e) {
		  e.preventDefault();
		  if ( $('.mobile-nav').hasClass('active') ) {
			   $('.mobile-nav').removeClass('active');
			   $('body').removeClass('scroll-lock');
			   $('#main-wrapper').removeClass('locked');
		  }else {
			   $('.mobile-nav').addClass('active');
			   $('body').addClass('scroll-lock');
			   $('#main-wrapper').addClass('locked');
		  }
	 });

	// MOBILE DROPDOWN
	$('.mobile-nav-menu li.primary-dropdown > a').click(function(e){
		e.preventDefault();
		if ( $(this).parent().hasClass('active') ) {
			$(this).parent().removeClass('active');
			$(this).parent().find('> ul').stop(true,false).slideUp();
		} else {
			$(this).parent().siblings().removeClass('active');
			$(this).parent().siblings().find('> ul').stop(true,false).slideUp();
			$(this).parent().addClass('active');
			$(this).parent().find('> ul').stop(true,false).slideDown();
		}
	});

	// TABS DROPDOWN
	$('.my-account .etabs-account li, .my-account .etabs-account').click(function() {
	   
	    if ($('.my-account .etabs-account li').hasClass('show-list')) {
	    	$('.my-account .etabs-account li').removeClass('fade-in');
		    	setTimeout(function() {
		    		$('.my-account .etabs-account li, .my-account .etabs-account').removeClass('show-list');
		    	}, 300);
	    }else{
		    	$('.my-account .etabs-account').addClass('show-list');
			    	setTimeout(function() {
			    		$('.my-account .etabs-account li').addClass('show-list fade-in');
			    }, 300);
	    }
	});
	
	// register areas
	if($('.page-account-register-step-3 .areas').length) {
		$('.page-account-register-step-3 .areas .area').click(function() {
			$(this).toggleClass('added');

			var therapyAreas = [];
			$('.page-account-register-step-3 .areas .area.added').each(function(key, value) {
				therapyAreas.push($(this).data('id'));
			});

			$('#field_preference').val(therapyAreas.join());

			return false;
		});
	}

	// back to top
	$('.a-top,.back-to-top a').click(function(e){
		e.preventDefault();
		$('html, body').animate({ scrollTop: 0 }, 600);
	});

	// TABS
	$('#my-account-tab,#my-order-tab').easytabs({
		updateHash : false,
		animationSpeed : 'fast'
	});

	// MY ACCOUNT THERAPY

	$('#preference li').click(function(){
		$(this).toggleClass('active');
	});

	$('.selected-sort #select').on('change', function(){
		$('#preference ul li').addClass('toggle-list');
		$('#preference ul li').removeClass('hide-active');
	});

	$('.selected-sort #unselect').on('change', function(){
		$('#preference ul li').removeClass('toggle-list');
		$('#preference ul li').addClass('hide-active');
	});

	// MY ACCOUNT EDIT
	var values = [];
	var salesRepPhotoSrc = '';
	$('#general li .edit').click(function(e){
		e.preventDefault();

		// cancel active tabs
		if($('#general li.active').length) {
			$('#general li.active .cancel-btn').click();
		}

		if ( !$(this).parent().parent().hasClass('active') ) {
			$(this).parent().parent().siblings().removeClass('active');
			$(this).parent().parent().siblings().find('.edit-mode').stop(true,false).slideUp();
			$(this).parent().parent().addClass('active');
			$(this).parent().parent().find('.edit-mode').stop(true,false).slideDown();

			// save values to var
			values = [];
			$(this).closest('li.active').find('input[type="text"], input[type="password"]').each(function() {
				values.push($(this).val());
			});

			// save original photo src to var
			$('#general #edit-upload-file').val('');
			if(salesRepPhotoSrc == '' && $('#general .photo-c-holder img').length) {
				salesRepPhotoSrc = $('#general .photo-c-holder img').attr('src');
			}

		}else{
			$(this).parent().parent().removeClass('active');
			$(this).parent().parent().find('.edit-mode').stop(true,false).slideUp();
		}
	});

	// ADD TO ORDER BUTTON
	$('.add-to-order').click(function() {
		$('.add-to-order').removeClass('disabled');
			$(this).animate({
				opacity: 0.6
			}, 500, "linear", function() {
			$(this).css("opacity", 1);
		});
	});

	$('#general li .cancel-btn').click(function(e){
		e.preventDefault();
		$('.edit-mode').stop(true,false).slideUp();

		// reset values
		$(this).closest('li.active').find('input[type="text"], input[type="password"]').each(function(index) {
			$(this).val(values[index]);
		});

		if($(this).closest('li.active')){
			$(this).closest('li.active').removeClass('active');
		}

		// sales rep photo
		if($('#general .photo-c-holder img').length && salesRepPhotoSrc != '') {
			setTimeout(function() {
				$('#general .photo-c-holder img').attr('src', salesRepPhotoSrc);
			}, 500);
		}

	});

	$('#edit-pass-pass1--2').focusin(function() {
		$('.form-item-pass-pass1').addClass('expand');
	});

	$('#grad-year').keyup(function(e) {
		var key = e.keyCode;
		var maxlength = $(this).val().length, value = $(this).val();
		if (!(key > 36 && key < 40)) {
			var output = "";
			document.getElementById(this.id).value = "";
			for (var i = 0; i < maxlength; i++) {
				if (!isNaN(Number(value.charAt(i)))) {
					output += value.charAt(i);	
				}
			}
			document.getElementById(this.id).value = output;
		}
	});

	$('#general #phone-number').on('change keyup keypress', function(e) {

		// trim not numbers
	    var _v = $(this).val();
		var _fv = _v.replace(/[^0-9\.]/g, '');
		if (_v != _fv) {
	       $(this).val(_fv);
	    }

	    // trim 0 as first number
	    var _v = $(this).val();
	    if(_v.length == 1 && parseInt(_v) == 0) {
	    	$(this).val('');
	    }

	    // reformat
		var _v = $(this).val();
		var _nfv = _v.replace(/[^0-9\.]/g, '');
		var _fv = _v.replace(/[^0-9\.\(\) ]/g, '');
		var _code = _fv.substring(0, 2);
		var _tel = _fv.substring(2, _fv.length);
		var _new = (_nfv.length > 2) ? '(' + _code + ') ' : '';
		_new += (_nfv.length > 2) ? _tel : _nfv;
		_new = (_new.length > 9) ? _new.substring(0, 9) + ' ' + _new.substring(9) : _new;
		if(_new.length > 14) _new = _new.substring(0, 14);

		if (_v != _new) {
	       $(this).val(_new);
	    }

	});

	// account setting validation
	$('#general form').submit(function() {
		var form = $(this);
		if(form.find('#phone-number').length) {
			// validate phone number
			if(form.find('#phone-number').val().length != 14) {
				form.find('#phone-number').addClass('invalid');
				return false;
			} else {
				form.find('#phone-number').removeClass('invalid');
			}
		} 

		return true;

	});

	// catch enter key to save
	$('#general input[type="text"], #general input[type="password"]').keyup(function(e) {
		if (e.keyCode == 13) {

			$(this).closest('li.active').find('.save-btn').mousedown();
			
			// re-save values 
			values = [];
			$(this).closest('li.active').find('input[type="text"], input[type="password"]').each(function() {
				values.push($(this).val());
			});
		}
	});

	// Medicines FAQs
	// $('#medicines .medicines-list ul li .medicines-btn').on('click', function(e){
	$('#medicines .medicines-list ul li .top-medicine-level').on('click', function(e){
		e.preventDefault();
		if($(this).closest('li').hasClass('active')) {
			$(this).closest('li').removeClass('active');
			$(this).closest('li').find('.sub-medicines-list').stop(true,false).slideUp();
			console.log("up");
		} else {
			$(this).closest('li').siblings().removeClass('active');
			$(this).closest('li').siblings().find('.sub-medicines-list').stop(true,false).slideUp();
			$(this).closest('li').addClass('active');
			$(this).closest('li').find('.sub-medicines-list').stop(true,false).slideDown();
			console.log("down");
		}

		return false;
	});

	// RESOURCES PAGE

	$('.resources-page .select-wrap select').each(function(){
	   var value = $(this).val();
	   if (value == "") $(this).css('opacity', .6);
	   else $(this).css('opacity', 1);
	});

	// my account photo change
	$('#general #edit-upload-file').change(function(e) {
		console.log(URL.createObjectURL(e.target.files[0]));
		if($(this).val() != '') {
			$('#general .photo-c-holder img').attr('src', URL.createObjectURL(e.target.files[0]));
		}
	});
	
});

$(window).load(function() {
	$('body').removeClass('preloading');
	repadBody();
	boxGrid();
	fadeContent();
	videoResize();
	resizeHome();
	resourcesImgResize();
	homeTiles();

	$(window).scroll();
});

$(window).resize(function() {
	// FUNCTION
	repadBody();
	boxGrid();
	parallax();
	videoResize();
	resizeHome();
	resourcesImgResize();
	homeTiles();

});

$(window).bind('orientationchange', function() {
	// FUNCTION
	parallax();
});

$(window).scroll(function(){
	// FUNCTION
	parallax();

	if ($(window).width() >= 768) {
		// if ($(window).scrollTop() > $(document).height() - $(window).height() - $('footer').outerHeight()) {
		if ($(window).scrollTop() > $(document).height() - $(window)[0].innerHeight - $('footer').outerHeight()) {
	       $('body').addClass('scroll');
	    } else {
	    	$('body').removeClass('scroll');
	    }
	};

	fadeContent();
});

function boxGrid() {
	// var innerli = $('.recent-activity li').size();

	// if (innerli == 4) {
	// 	$('.recent-activity li:last-child').css('display','none');
	// 	$('.recent-activity-wrap').parents().removeClass('recent-activity-wrap');
	// }

	// if ($(window).width() >= 768) {
	// if (window.innerWidth >= 768) {
	// 	if (innerli < 4) {
	// 		var mxHeight = 0;
	// 		$('.home-column .box-grid-col').css({'height':'auto'});
	// 		$('.home-column .box-grid-col').each(function(){	
	// 			if($(this).height() > mxHeight) 
	// 			   mxHeight = $(this).height();  
	// 		});  
	// 		$('.home-column .box-grid-col').height(mxHeight);
	// 		$('.recent-activity-wrap').parents().removeClass('recent-activity-wrap');
	// 	}
	// } else {
	// 	$('.home-column .box-grid-col').css({'height':'auto'});
	// };

	// if (innerli > 3) {
	// 	$('.recent-activity').parent().addClass('recent-activity-wrap');
	// }

	// box grid
	var mxHeight = 0;
    $('.docs-details').css({'height':'auto'});
    $('.docs-details').each(function(){
        if($(this).outerHeight(false) > mxHeight)
           mxHeight = $(this).outerHeight(false);
    });
    $('.docs-details').css({'height' : mxHeight});

}

function boxGrid2() {
	var innerli = $('.resources li').size();

	// if (innerli < 4) {
	// 	$('.resources li:last-child').css('display','none');
	// }
	
	
	// if ($(window).width() >= 768) {
	if (window.innerWidth >= 768) {
		if (innerli < 4) {	
			var mxHeight = 0;
			$('.home-column .box-grid-col-2').css({'height':'auto'});
			$('.home-column .box-grid-col-2').each(function(){	
				if($(this).height() > mxHeight) 
				   mxHeight = $(this).height();  
			});  
			$('.home-column .box-grid-col-2').height(mxHeight);

			// IMAGE HEIGHT
			var boxH = $('.home-column-2 > .row').height();
			$('.home-column .absoute-img').css('height',boxH);
		}
	} else {
		$('.home-column .box-grid-col-2').css({'height':'auto'});
	}

	if (innerli > 3) {
		var boxMarginTop = $('.box-grid-col-2').innerHeight();
		$('.webcast-wrap').css('margin-top',-boxMarginTop + 400);
		$('.webcast-column').css({
			'float' : 'right'
		});
		$('.events-column').css({
			'display' : 'none'
		});
	}
}

// PARALLAX BACKGROUND

function parallax() {
	parallaxBackground( $('.home-medicines'), 0.3 );
}

function parallaxBackground( imgDiv, multiplier ) {

	if ( imgDiv.size() > 0 ) {
		var wH = window.innerHeight ? window.innerHeight : $(window).height();
		var imgPar = imgDiv;
		var imgParPercY = ( imgPar.offset().top - $(window).scrollTop() ) / imgPar.outerHeight(false);
		var parallaxHeight = imgPar.outerHeight(false);
		var newH = Math.round( (imgParPercY) * parallaxHeight * multiplier/4 );
		imgDiv.css({
			'background-position': '50% ' + newH + 'px'
		});
	}

}

function initCustomForm() {
	$('select.custom-select').each(function() {
		$(this).wrap('<div class="custom-select-wrapper" />');
		$(this).before('<div class="custom-select-display" />');
		$(this).change(function() {
			$(this).siblings('.custom-select-display').text( $(this).find('option:selected').text() );
		});
		$(this).keyup(function() {
			$(this).siblings('.custom-select-display').text( $(this).find('option:selected').text() );
		});
		$(this).change();
	});

	$('label.custom-checkbox').each(function() {
		if ( $(this).find('custom-checkbox-display').size() <= 0 ) {
			$(this).append('<div class="custom-checkbox-display" />');
		}
	});
	$('label.custom-radio').each(function() {
		if ( $(this).find('custom-radio-display').size() <= 0 ) {
			$(this).append('<div class="custom-radio-display" />');
		}
	});
}

function fadeContent(){
	$('.resources-page-fade').each( function(i){
	    
	    var bottom_of_object = $(this).offset().top + $(this).outerHeight();
	    var bottom_of_window = $(window).scrollTop() + $(window).height();
	    
	    if( bottom_of_window > bottom_of_object ){
	        $(this).addClass('resources-page-fade-content');
	    }
	}); 
}

function videoResize() {
	var ratioW = 16;
	var ratioH = 9;
	var videoW = $(".video-holder iframe").width();


	var vRatio = ratioH/ratioW;
	var computedHeight = videoW * vRatio;


	$('.video-holder iframe').css({
		'height' : computedHeight
	});
}

function resizeHome() {

	var minH = 383;
	var maxH = 1000;
	// var newH = $(window).height() - $('#header').height();
	var newH = $(window).height() - $('header').height();
	newH = (newH < minH) ? minH : newH;
	newH = (newH > maxH) ? maxH : newH;
	$('#home-hero, #home-hero-slider').css('height', newH);
	//$('#home-hero .hero-container .table').css('height', newH-113);
	$('#home-hero .hero-container .table .table-cell').css('margin-top', ((newH-113)-$('#home-hero .hero-container .table .table-cell').height())/2 )

	$('#home-hero').css('margin-top', -(newH));

	var wH = window.innerHeight ? window.innerHeight : $(window).height();
	// $('#main-wrapper, #account-verification').css({ 'min-height': wH-$('#header').height()-$('#footer-wrapper').height()  });
	$('#main-wrapper, #account-verification').css({ 'min-height': wH-$('header').height()-$('footer').height()  });

}

function resourcesImgResize() {

	if (window.innerWidth < 992 && window.innerWidth > 767) {
		$('.resources-page .listings li').each(function(){
			var _list = $(this),
				_targetHeight = _list.find('.row').height();
				
			_list.find('.resources-img').css({
				'min-height': _targetHeight+'px'
			});
		});
	} else {
		$('.resources-page .resources-img').removeAttr('style');
	}
}

var newsHeadingsArr = [];
var newsExcerptArr = [];
function homeTiles() {
	var body = $('body');
	if (
		window.innerWidth >= 768 && 
		body.hasClass('front') &&
		body.hasClass('logged-in')
	) {
		// recent activity
		var recent = $('.recent-activity');
		if(recent.length) {
			var recentItem = recent.find('li');
			var wrapper = $('.activity-row');
			if(recentItem.length > 3) {
				wrapper.addClass('overflow');
			} else {
				wrapper.removeClass('overflow');
			}

			// check for overflows
			var recentWrap = recent.parent();
			$('li', recent).removeClass('hidden');
			while(recentWrap[0].offsetHeight < recentWrap[0].scrollHeight) {
				// hide last if overflow
				$('li:visible:last', recent).addClass('hidden');
			}
		}

		// resources
		var resources = $('.resources');
		var upcomingList = $('.upcoming-events li');
		if(resources.length) {
			var resourcesItem = resources.find('li');
			var wrapper = $('.resources-row');

			// add 'overflow' class when - resources length > 3 or no upcoming events
			if(resourcesItem.length > 3 || !upcomingList.length) {
				wrapper.addClass('overflow');
			} else {
				wrapper.removeClass('overflow');
			}

			// check for overflows
			var resourcesWrap = resources.parent();
			$('li', resources).removeClass('hidden');
			while(resourcesWrap[0].offsetHeight < resourcesWrap[0].scrollHeight) {
				// hide last if overflow
				$('li:visible:last', resources).addClass('hidden');
			}
		}

		/** restrict news headings and excerpt to 3 lines each **/

		// heading

		// get all headings if not set
		if(!newsHeadingsArr.length) {
			$('.news h2').each(function() {
				newsHeadingsArr.push($(this).html());
			});
		} 

		// get all excerpt if not set
		if(!newsExcerptArr.length) {
			$('.news .excerpt > p').each(function() {
				newsExcerptArr.push($(this).html());
			});
		} 

		// check news headings height
		$('.news h2').each(function(i) {
			var headingObj = $(this);
			headingObj.html(newsHeadingsArr[i]);

			// while greater than three lines, trim
			while(headingObj.innerHeight() > parseInt(headingObj.css('lineHeight')) * 3) {
				headingObj.html(headingObj.html().substring(0, headingObj.html().lastIndexOf(' ')) + '...');
			}

		});

		// check news excerpt height
		$('.news .excerpt > p').each(function(i) {
			var excerptObj = $(this);
			excerptObj.html(newsExcerptArr[i]);

			// while greater than three lines, trim
			while(excerptObj.innerHeight() > parseInt(excerptObj.css('lineHeight')) * 3) {
				excerptObj.html(excerptObj.html().substring(0, excerptObj.html().lastIndexOf(' ')) + '...');
			}

		});


	} else { // if mobile

		if (
			body.hasClass('front') &&
			body.hasClass('logged-in')
		) { // if front page mobile
		
			if(newsHeadingsArr.length) {
				$('.news h2').each(function(i) {
					$(this).html(newsHeadingsArr[i]);
				});
			}

			if(newsExcerptArr.length) {
				$('.news .excerpt > p').each(function(i) {
					$(this).html(newsExcerptArr[i]);
				});
			}

		}

	}
}

function repadBody() {
	$('#site-wrapper').css({
		paddingTop: $('header').outerHeight()
	})

	$('.mobile-nav-menu').css({
		top: $('#au-header').outerHeight(),
		height: window.innerHeight - $('#au-header').outerHeight()
	})
}

